for (var i = 0; i < 100; i++) {
    message = "";

    if (i % 3 === 0 || i % 5 === 0) {
        if (i % 3 === 0) {
            message += "Fizz";
        } 

        if (i % 5 === 0) {
            message += "Buzz";
        }
    } else {
        message = i;
    }

    console.log(message);
}
